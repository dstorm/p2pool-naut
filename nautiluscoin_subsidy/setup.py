from distutils.core import setup
from distutils.extension import Extension

setup(name="nautiluscoin_subsidys",
    ext_modules=[
        Extension("nautiluscoin_subsidy", ["nautiluscoin_GetBlockBaseValue.cpp"],
        libraries = ["boost_python"])
    ])
