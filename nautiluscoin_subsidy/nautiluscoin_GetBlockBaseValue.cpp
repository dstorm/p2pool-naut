#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <math.h> 

static const int64_t COIN = 100000000;
static const int64_t nDiffChangeTarget = 50; // Reward effective @ block 50
static const int64_t nDiffFinalTarget = 105400; // Last Minted Reward Block @ block 105,400
static const int64_t patchBlockRewardDuration = 10080; // 10080 blocks main net change

int64_t GetNAUTSubsidy(int nHeight) {
   // thanks to RealSolid & WDC for helping out with this code
   int64_t qSubsidy = 161*COIN;
   int blocks = nHeight - nDiffChangeTarget;
   int weeks = (blocks / patchBlockRewardDuration)+1;
   //decrease reward by 1.0% every week
   for(int i = 0; i < weeks; i++)  qSubsidy -= (qSubsidy/100);
   return qSubsidy;
}

int64_t GetBlockBaseValue(int nHeight)
{
   int64_t nSubsidy = COIN;
   
   if(nHeight < nDiffChangeTarget) {
      //Reward is 3236 1st 50 blocks.
      nSubsidy = 3236 * COIN;
   } else if ( nHeight < nDiffFinalTarget ){
      nSubsidy = GetNAUTSubsidy(nHeight);
   } else {
     //Drop reward to zero once total amount has been minted
     nSubsidy = 0;
   }

   return nSubsidy;
}

#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
using namespace boost::python;
 
BOOST_PYTHON_MODULE(nautiluscoin_subsidy)
{
    def("GetBlockBaseValue", GetBlockBaseValue);
}

